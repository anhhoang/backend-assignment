import { setupServer } from 'msw/node';
import * as handlers from './handlers';

export const fail = setupServer(...handlers.allFail);
export const normal = setupServer(...handlers.normal);
