import axios from 'axios';

export type ServerType = {
  url: string;
  priority: number;
};

export const findServer = async (servers: ServerType[]) => {
  const serverStatusPromises = servers.map((server) =>
    axios
      .get(server.url, { timeout: 5000 })
      .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
          return { ...server, online: true };
        }
        throw new Error();
      })
      .catch(() => ({ ...server, online: false }))
  );

  const results = await Promise.all(serverStatusPromises);
  const onlineServers = results.filter((server) => server.online);
  if (onlineServers.length === 0) {
    throw new Error('No servers are online.');
  }

  return onlineServers.reduce((prev, current) => (prev.priority < current.priority ? prev : current));
};
