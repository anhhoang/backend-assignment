import { servers } from './data';
import { findServer } from './health-check';

findServer(servers).then((result) => console.table(result));
