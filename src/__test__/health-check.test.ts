import { servers } from '../data';
import { findServer } from '../health-check';
import * as server from './mocks';

describe('Health check server', () => {
  test('Returns a server with the lowest priority that is online', async () => {
    server.normal.listen();
    try {
      const response = await findServer(servers);
      expect(response).toEqual({
        url: 'http://app.scnt.me',
        priority: 3,
        online: true,
      });
    } finally {
      server.normal.resetHandlers();
      server.normal.close();
    }
  });

  test('Throws an error if no servers are online', async () => {
    server.fail.listen();
    try {
      await findServer(servers);
    } catch (error) {
      expect(error).toEqual(new Error('No servers are online.'));
    } finally {
      server.fail.resetHandlers();
      server.fail.close();
    }
  });
});
