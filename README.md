## Getting started

This project require nodejs version >= 20.

To run this project, 

```
npm install
npm run build
npm start
```

To test ( make sure you ran `npm install`)
```
npm test
```
