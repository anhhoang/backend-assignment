import { http, HttpResponse } from 'msw';
import { servers } from '../../data';

export const allFail = servers.map((server, index) =>
  http.get(server.url, () =>
    (index & 1) == 0
      ? HttpResponse.error()
      : HttpResponse.text('Internal Server Error', {
          status: 500,
        })
  )
);

export const normal = servers.map((server, index) =>
  http.get(server.url, () =>
    server.priority <= 2
      ? HttpResponse.error()
      : HttpResponse.text('Success', {
          status: 200,
        })
  )
);
